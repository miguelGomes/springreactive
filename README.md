## Introduction

This project contains example of spring implementations.

- Spring Routes

- Spring Reactive

## Compile

mvn clean install


## Start

*mvn spring-boot:run*

Service will be listening on http://localhost:8080

#### Endpoints

**GET** /person
 
**GET** /person/{id}
 
**POST** /person
 

#### Examples

curl http://localhost:8080/person