package com.test.springreactive.db.repository;

import com.test.springreactive.db.entity.Person;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface PersonRepository extends ReactiveMongoRepository<Person, String>{

    Flux<Person> findByName(String name);

}
