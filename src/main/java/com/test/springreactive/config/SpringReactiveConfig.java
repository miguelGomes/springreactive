package com.test.springreactive.config;

import com.test.springreactive.db.entity.Person;
import com.test.springreactive.db.repository.PersonRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.reactive.function.server.RouterFunction;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Configuration
public class SpringReactiveConfig {

    @Bean
    RouterFunction<?> routes(PersonRepository personRepository) {
        return nest(path("/person"),
                route(GET("/{id}"),
                        request -> ok().body(personRepository.findById(request.pathVariable("id")), Person.class))
                        .andRoute(method(HttpMethod.POST),
                                request -> {
                                    personRepository.insert(request.bodyToMono(Person.class)).subscribe();
                                    return ok().build();
                                })
                .andRoute(method(HttpMethod.GET),
                        request -> ok().body(personRepository.findAll(), Person.class))
        );
    }


}
